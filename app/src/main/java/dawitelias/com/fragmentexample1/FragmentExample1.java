package dawitelias.com.fragmentexample1;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class FragmentExample1 extends Activity {
    private MyListFragment myListFragment;
    private FragmentTwo fragmentTwo;
    private Button listButton;
    private Button fragmentTwoButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_example1);
        listButton = (Button)findViewById(R.id.listButton);
        listButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFragment("List");
            }
        });

        fragmentTwoButton = (Button)findViewById(R.id.fragmentTwoButton);
        fragmentTwoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFragment("Two");
            }
        });

        // normally would need to keep track of which fragment is being
        // displayed for rotation purposes
        loadFragment("List");
    }

    private void loadFragment(String which) {
        if (which.equals("List")) {
            // get the MyListFragment
            myListFragment = new MyListFragment();

            // set the change listener
            myListFragment.setItemChangedListener(itemChangedListener);
            // note: no transition or backstack -- but clear backstack
            // pop everything off backstack
            getFragmentManager().popBackStack(null, getFragmentManager().POP_BACK_STACK_INCLUSIVE);
            // "here's all the things that I want to happen when I put things on"
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.the_info, myListFragment);
            ft.commit();
        } else if (which.equals("Two")) {
            // get the Fragment Two
            fragmentTwo = new FragmentTwo();
            // note: no transition or backstack -- but clear backstack
            // pop everything off backstack
            getFragmentManager().popBackStack(null, getFragmentManager().POP_BACK_STACK_INCLUSIVE);
            // "here's all the things that I want to happen when I put things on"
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.the_info, fragmentTwo);
            ft.commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_fragment_example1, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // listener for the list fragment
    private MyListFragment.ItemChangedListener itemChangedListener = new MyListFragment
            .ItemChangedListener() {
        @Override
        public void onSelectedItemChanged(String itemNameString) {
            // create and show the detail fragment
            DetailFragment details = DetailFragment.newInstance(itemNameString);
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.anim.fragment_animation_fade_in, R.anim.fragment_animation_fade_out);
            ft.replace(R.id.the_info, details);
            ft.addToBackStack(null);
            ft.commit();
        }
    };
}
